import {postcodes} from "./postcodes"


const dropdown = document.getElementById('locality')
const options = document.getElementById('options')
var highlighted = -1


const populate = () => {
    postcodes.forEach( ( place ) => {
        options.appendChild( createSelectableElement( place ) )
    })
}

const createSelectableElement = ( place ) => {
    const textNode = document.createTextNode( place )
    const container = document.createElement('div')
    //container.classList.add('visible')
    container.appendChild( textNode )
    container.addEventListener( "click", ( event ) => {
        selectValue(event.target.innerText)
    } )
    return container
}

const selectValue = ( value ) => {
    dropdown.value = value
    dropdown.select()
    options.innerHTML = ""
    dropdown.blur()

}

dropdown.addEventListener('focus', ( e ) => {
    if( options.children.length !== 0 ) return
    populate()

} )

dropdown.addEventListener( 'keyup', ( e ) => {

    if( options.children.length === 0 ) {
        populate()
    }

    const children = options.children

    for( var i=0; i < children.length; i++ ) {
        if ( children[i].innerText.toLocaleLowerCase().includes( dropdown.value.toLocaleLowerCase().trim() ) ) {
            children[i].classList.add('visible')
        } else {
            children[i].classList.remove('visible')
        }
    }

})

dropdown.addEventListener('keydown', ( e ) => {
// first increment decrement then highlight!
    const elements = document.querySelectorAll('#options .visible')

    if( highlighted > elements.length -1 ) {
        highlighted = elements.length -1
    }

    if( e.key == "ArrowDown") {
        if( highlighted >= 0 ) {
            elements[highlighted].classList.remove('highlight')
        }
        highlighted++
        if( highlighted > elements.length - 1 ) {
            highlighted = 0
        }
        elements[highlighted].classList.add('highlight')
        elements[highlighted].scrollIntoView({block: "center", inline: "nearest"} )

    }

    if( e.key == "ArrowUp") {
        elements[highlighted].classList.remove('highlight')
        highlighted--
        if( highlighted < 0 ) {
            highlighted = elements.length - 1
        }
        elements[highlighted].classList.add('highlight')
        elements[highlighted].scrollIntoView({block: "center", inline: "nearest"} )
    }

    if( e.key == "Enter" ) {
        selectValue(elements[highlighted].innerText)
    }
})