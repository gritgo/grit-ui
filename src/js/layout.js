
const header = document.querySelector('header')

const footer = document.querySelector('footer')

const main = document.querySelector('main')

// console.log(header.clientHeight, footer.clientHeight)

if( header ) {
    main.style.paddingTop = header.clientHeight + "px"
    const headerObserver = new ResizeObserver( entries => main.style.paddingTop = entries[0].target.clientHeight + "px" )
    headerObserver.observe(header)
}

if( footer ) {
    main.style.paddingBottom = footer.clientHeight + 10 + "px"
    const footerObserver = new ResizeObserver( e => main.style.paddingBottom = e[0].target.clientHeight + 10 + "px")
    footerObserver.observe(footer)
}





