const toggleNav = document.querySelectorAll('.navigation-toggle')

const menu = document.querySelector('nav')

toggleNav.forEach( (element) => {
    element.addEventListener( 'click', () => {
        menu.classList.toggle('opened')
    })
})

