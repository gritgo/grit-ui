// This component is intended for testing purposes
// If you need to mock various events you put them here

const slow = document.querySelector('.slow-opener')

var count = 0;
var goto;


slow.addEventListener("click", function(e){
    e.preventDefault()
    goto = this.href
    count ++
    if(count > 1) {
        goto = "/fail.html"
    }
    //delayLoad()

})

function delayLoad() {
    console.log(count)
    setTimeout(() => {      
        window.location = goto
    }, 3000, goto)
}
