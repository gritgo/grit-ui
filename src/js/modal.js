const modals = document.querySelectorAll('.modal-button')

modals.forEach( (element) => {
    element.addEventListener('click', (event) => {
        const wrapper = document.querySelector('.modal-wrapper')
        
        wrapper.style.display = "flex"

        const modal = document.getElementById( event.target.dataset.modalId ) 
        
        modal.style.display="block"

        modal.querySelector('.close-modal').addEventListener('click', () => {
            modal.style.display="none"
            wrapper.style.display="none"
        })
    })
})