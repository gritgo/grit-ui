const slowLink = document.querySelector('.slow-opener')

slowLink.addEventListener('click', overlay)

function overlay() {
    const overlay = document.createElement("div")
    overlay.innerHTML = "<div>Please, wait while the request finishes.</div>"
    overlay.style.top = "0"
    overlay.style.bottom = "0"
    overlay.style.left = "0"
    overlay.style.right = "0"
    overlay.style.position = "absolute"
    overlay.style.backgroundColor = "rgb(220, 64, 126, .9)"
    overlay.style.color ="#fff"
    overlay.style.fontSize = "2rem"
    overlay.style.fontWieght = "700"
    overlay.style.display = "flex"
    overlay.style.justifyContent = "center"
    overlay.style.alignItems = "center"
    overlay.style.zIndex = 10000
    document.body.appendChild(overlay)
}