const sliders = document.querySelectorAll('.inside-wc-product-image')

sliders.forEach( (e, index) => {
    
    let intervalId = false
    let images = e.querySelectorAll('img')

    //console.log(images.length)
    images[0].classList.add('active')

    e.addEventListener('mouseover', () => {
        intervalId = runSlider(images)
    })
    e.addEventListener('mouseout', () => {
        stopSlider(intervalId)
    })

    if(isTouchDevice()) {

        if(isInViewport(e.getBoundingClientRect()) && ! intervalId ) {
            intervalId = runSlider(images)
        } 
        
        window.addEventListener('touchend', (event) => {
            console.log(intervalId)
            if(isInViewport(e.getBoundingClientRect()) && ! intervalId )  {
                //console.log('Should run')
                intervalId = runSlider(images)
            } else {
                stopSlider(intervalId)
                intervalId = false;
            }
        })
        //console.log(window.visualViewport)
    }
})


function runSlider(images) 
{

    let counter = 1
    
    return setInterval( () => {    
        //console.log("Index" + index + ": " + counter)
        images.forEach((e) => {
            e.classList.remove('active')
        } )
        images[counter].classList.add('active')
    
        if( counter < images.length - 1) {
            counter ++
        } else {
            counter = 0
        }
    }, 1000)
}

function stopSlider(intervalId)
{
    clearInterval(intervalId)

}

function isInViewport(rect)
{
    return rect.top > 0 && rect.left > 0 && rect.bottom < window.innerHeight && rect.right < window.innerWidth
}

function isTouchDevice()
{
    return 'ontouchstart' in window
}