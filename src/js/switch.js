const switchelement = document.querySelectorAll('.switch')

switchelement.forEach((item, index) => {

    var checkbox = item.querySelector('input')

    //console.log(checkbox.checked)

    if(checkbox.checked) {
        item.classList.add('on');
    }

    item.addEventListener('click',(e) => {        
        e.target.classList.toggle('on')
        checkbox.checked = !checkbox.checked
        console.log(checkbox.checked)
    })

})